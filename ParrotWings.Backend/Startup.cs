using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using ParrotWings.Backend.Services.Helpers;
using ParrotWings.Backend.Services.Services;
using ParrotWings.Core;
using ParrotWings.Core.Model;

namespace ParrotWings.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddDbContext<PWContext>(opt =>
                                               opt.UseInMemoryDatabase("ParrotWingsDataBase"));

            //services.AddCors();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ParrotWings.Backend", Version = "v1" });
            });

            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = "Bearer";
                    options.DefaultChallengeScheme = "Bearer";
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKey = SecurityKeyHelper.GetSecuirtyKey(),
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidIssuer = Config.ValidIssuer,
                        ValidAudience = Config.ValidAudience,
                        ClockSkew = TimeSpan.FromTicks(0)
                    };
                });

            services.AddScoped<AccountService>();
            services.AddScoped<TokenService>();
            services.AddScoped<TransactionsService>();
            services.AddScoped<TransactionHistoryService>();
            services.AddScoped<LogService>();            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ParrotWings.Backend v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            /*
            app.UseCors(
                options => options.SetIsOriginAllowed(x => _ = true).AllowAnyMethod().AllowAnyHeader().AllowCredentials()
            );*/

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
