﻿using System.Collections.Generic;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Backend.Services.Extensions
{
    public static class AppExtensions
    {
        public static UserMeta ToModel(this User model) =>
            new UserMeta
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                Balance = model.Balance
            };

        public static IEnumerable<UserMeta> ToCollection(this IEnumerable<User> users)
        {
            List<UserMeta> userMetas = new List<UserMeta>();
            if (users == null) return userMetas;

            foreach (var user in users)
            {
                userMetas.Add(user.ToModel());
            }
            return userMetas;
        }

    }
}
