﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Responses;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Backend.Services.Services
{
    public class TransactionHistoryService
    {
        private readonly PWContext _context;
        private readonly LogService _logService;

        public TransactionHistoryService(PWContext context, LogService logService)
        {
            _context = context;
            _logService = logService;
        }


        internal async Task<GetUserTransactionsResponse> GetUserHistory(long userId)
        {
            try
            {
                var history = await _context.TransactionHistory.Where(h => h.UserId == userId)
                                                  .ToListAsync();
                return new GetUserTransactionsResponse
                {
                    IsSuccess = true,
                    TransactionHistory = history
                };

            }
            catch (Exception ex)
            {
                _logService.Write($"Getting user error: userId = {userId} {ex.Message} {ex.StackTrace}");
                return new GetUserTransactionsResponse()
                {
                    IsSuccess = false,
                    Error = "Could not get user's transactions"
                };
            }
        }

        internal async Task<TransactionHistory> CreateHistory(long transactionId,
                                                                            long userSenderId,
                                                                            long userReceiverId,
                                                                            decimal balance,
                                                                            decimal amount,
                                                                            bool isSenderHistory = false
                                                                            )
        {
            var history = new TransactionHistory
            {
                TransactionId = transactionId,
                Amount = amount,
                Balance = balance,
                SenderId = userSenderId,
                ReceiverId = userReceiverId,
                UserId = isSenderHistory ? userSenderId : userReceiverId,
                CreatedAt = DateTime.UtcNow
            };
            _context.TransactionHistory.Add(history);
            await _context.SaveChangesAsync();
            return history;
        }
    }
}
