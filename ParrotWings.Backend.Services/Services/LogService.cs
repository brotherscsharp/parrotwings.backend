﻿using System;
using NLog;

namespace ParrotWings.Backend.Services.Services
{
    public class LogService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public LogService()
        {
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "ParrotWings.Backend.log" };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

            // Apply config           
            LogManager.Configuration = config;
        }

        public void Write(Exception exception)
        {
            logger.Error($"{exception.Message} {exception.StackTrace}");
        }

        public void Write(string message)
        {
            logger.Info($"{message}");
        }
    }
}
