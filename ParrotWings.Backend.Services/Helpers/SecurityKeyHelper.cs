﻿using System;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using ParrotWings.Core;

namespace ParrotWings.Backend.Services.Helpers
{
    public class SecurityKeyHelper
    {
        private static RsaSecurityKey _rsaSecurityKey = null;
        public static RsaSecurityKey GetSecuirtyKey()
        {
            if (_rsaSecurityKey == null)
            {
                var rsa = RSA.Create();
                rsa.ImportRSAPrivateKey(Convert.FromBase64String(Config.PrivateKey), out _);
                _rsaSecurityKey = new RsaSecurityKey(rsa);
            }

            return _rsaSecurityKey;
        }
    }
}
