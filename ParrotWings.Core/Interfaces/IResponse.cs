﻿namespace ParrotWings.Core.Interfaces
{
    public interface IResponse
    {
        bool IsSuccess { get; set; }
        string Error { get; set; }
    }
}
