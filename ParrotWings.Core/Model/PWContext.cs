﻿using Microsoft.EntityFrameworkCore;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Core.Model
{
    public class PWContext : DbContext
    {
        public PWContext(DbContextOptions<PWContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionHistory> TransactionHistory { get; set; }
    }
}
