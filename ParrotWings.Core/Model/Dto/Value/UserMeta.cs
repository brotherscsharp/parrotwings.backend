﻿namespace ParrotWings.Core.Model.Dto.Value
{
    public class UserMeta
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
    }
}
