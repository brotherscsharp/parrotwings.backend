﻿using System;
namespace ParrotWings.Core.Model.Dto.Value
{
    public class TransactionHistory
    {
        public long Id { get; set; }
        public long TransactionId { get; set; }
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
