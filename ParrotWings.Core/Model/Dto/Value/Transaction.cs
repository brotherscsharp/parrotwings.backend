﻿namespace ParrotWings.Core.Model.Dto.Value
{
    public class Transaction
    {
        public long Id { get; set; }
        public decimal Amount { get; set; }
        public long ReceiverId { get; set; }
        public long SenderId { get; set; }
    }
}
