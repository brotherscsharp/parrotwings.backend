﻿using System.Collections.Generic;
using ParrotWings.Core.Interfaces;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Core.Model.Dto.Responses
{
    public class GetUsersResponse : IResponse
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
        public IEnumerable<UserMeta> Users { get; set; }
    }
}
