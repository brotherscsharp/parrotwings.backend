﻿using System.Collections.Generic;
using ParrotWings.Core.Interfaces;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Core.Model.Dto.Responses
{
    public class GetUserTransactionsResponse : IResponse
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
        public IEnumerable<TransactionHistory> TransactionHistory { get; set; }
    }
}
