﻿namespace ParrotWings.Core.Model.Dto.Requests
{
    public class CreateTransactionRequest
    {
        public long UserId { get; set; }
        public decimal Amount { get; set; }
    }
}
