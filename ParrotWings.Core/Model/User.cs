﻿namespace ParrotWings.Core.Model
{
    public class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RepeatePassword { get; set; }
        public decimal Balance { get; set; }
    }
}
